__author__ = "hw56@indiana.edu"

import os
import scrapy
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem

user_agent_rotator = UserAgent(software_names=[SoftwareName.CHROME.value],
                               operating_systems=[OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value],
                               limit=500)
# a directory to save files
save_dir = 'resource'

if not os.path.exists(save_dir):
    os.makedirs(save_dir)


class QuotesSpider(scrapy.Spider):
    name = "poemanalysis"

    def start_requests(self):

        start_urls = ['https://poemanalysis.com/poet-directory/']
        for url in start_urls:
            yield scrapy.Request(url=url,
                                 headers={'User-Agent': user_agent_rotator.get_random_user_agent()},
                                 callback=self.parse_poet_url)

    def parse_poet_url(self, response):
        for poet_url in response.css(".az-columns a::attr(href)").getall():
            yield scrapy.Request(poet_url, callback=self.parse_num_pages,
                                 headers={'User-Agent': user_agent_rotator.get_random_user_agent()},
                                 dont_filter=True)

    def parse_num_pages(self, response):
        # single page, e.g., https://poemanalysis.com/ada-limon/
        if not response.css("#nav-below").get():
            yield scrapy.Request(response.url,
                                 headers={'User-Agent': user_agent_rotator.get_random_user_agent()},
                                 callback=self.parse_single_poem_url)
        # multiple pages, e.g., https://poemanalysis.com/emily-dickinson/
        else:
            max_page_num = max([int(u.split("/")[-2]) for u in response.css(".nav-links a::attr(href)").getall()])
            for page_num in range(1, max_page_num + 1):
                poem_url = response.url + f"page/{str(page_num)}/"
                yield scrapy.Request(poem_url,
                                     headers={'User-Agent': user_agent_rotator.get_random_user_agent()},
                                     callback=self.parse_single_poem_url)

    def parse_single_poem_url(self, response):
        for poem_url in response.css(".entry-title a::attr(href)").getall():
            yield scrapy.Request(poem_url,
                                 headers={'User-Agent': user_agent_rotator.get_random_user_agent()},
                                 callback=self.parse_poem_page)

    def parse_poem_page(self, response):

        content = response.css("#primary").get()
        poet = response.url.split("/")[-3]
        poem = response.url.split("/")[-2]
        filename = f'{poet}--{poem}.html'
        with open(os.path.join(save_dir, filename), 'w') as f:
            f.write(content)
        self.log(f'Saved file {filename}.')
